import Dependencies._

lazy val scalaCompilerOptions = Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Xlint"
)

lazy val root = (project in file("."))
  .settings(
    inThisBuild(
      List(
        organization := "com.invitae",
        scalaVersion := "2.13.0",
        version := "0.1.0-SNAPSHOT"
      )
    ),
    scalacOptions ++= scalaCompilerOptions,
    name := "CohortAnalysisService",
    scalafmtOnCompile := true,
    libraryDependencies ++= Seq(
      compilerPlugin(Libraries.betterMonadicFor),
      Libraries.cats,
      Libraries.catsEffect,
      Libraries.doobieCore,
      Libraries.doobieH2,
      Libraries.doobieHikari,
      Libraries.flyway,
      Libraries.fs2,
      Libraries.fs2IO,
      Libraries.h2,
      Libraries.http4sDsl,
      Libraries.http4sServer,
      Libraries.http4sCirce,
      Libraries.http4sClient,
      Libraries.jodaTime,
      Libraries.log4cats,
      Libraries.logback,
      Libraries.pureConfig,
      Libraries.doobieTest % Test,
      Libraries.scalaCheck % Test,
      Libraries.scalaMock  % Test,
      Libraries.scalaTest  % Test
    )
  )
