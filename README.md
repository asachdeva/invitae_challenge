# Cohort Analysis Service
This service performs a cohort analysis on sample customer and order csv data.  The data gets ingested into an H2 DB (in memory). 
It does a cohort analysis on the data and produces the result into an html page, with the cohorts segmented into weeks in the default timezone ('US/Pacific').

Note: Since the data is held in memory it ingests the csv files every time the application is launched but if the data
would need to be persisted to survive an app restart I would persist DB on disk and a do a upsert operation on the data

For larger data files, I would use .mapChunk function in fs2 Streams API to chunk (or batch) inserts into the DB

## Compile Service

sbt compile

## Run Tests

sbt test

## Run Service

sbt run

## Tech-Stack + Dependencies
- [Cats](https://typelevel.org/cats/) for pure FP
- [Cats-Effect](https://typelevel.org/cats-effect/) for IO Monad and Concurrency
- [Circe](https://circe.github.io/circe/) for json serialization
- [Doobie](https://github.com/tpolecat/doobie) for database access using HikariTransactor against H2
- [FS2](https://fs2.io) for Streams and IO (reading and writing files)
- [Http4s](http://http4s.org/) the web server
- [ScalaTest](https://www.scalatest.org/) for testing
- Tagless Final for the core domain.
- Latest on Scala..2.13.0

## Usage
- Service runs on http://localhost/cohort/analysis 
- Issue GET against REST API using samples for different timezones like
```
curl "localhost/cohort/analysis?tz=pt -X GET"
curl "localhost/cohort/analysis?tz=mt -X GET"
curl "localhost/cohort/analysis?tz=ct' -X GET"
curl "localhost/cohort/analysis?tz=et' -X GET"
```
- In case bad input (unsupported unit) a "Bad Input" will be displayed
- In case a bad URL is entered it will respond with a NotFound




