-- Customers table
CREATE TABLE customers
(
    id  INT PRIMARY KEY,
    created TIMESTAMP NOT NULL
);

-- Create created Index for customer table
CREATE INDEX idx_customer_created ON customers(created);

-- Orders Table
CREATE TABLE orders
 (
	id INT PRIMARY KEY,
	order_number INT NOT NULL,
	user_id INT NOT NULL,
	created TIMESTAMP NOT NULL
-- 	FOREIGN KEY(user_id) REFERENCES customers(id)
);

-- Create created Index for order table
CREATE INDEX idx_order_created ON orders(created);

