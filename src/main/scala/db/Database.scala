package db

import cats.effect._
import doobie._
import doobie.implicits._
import config._
import org.flywaydb.core.Flyway
import doobie.hikari._
import doobie.util.ExecutionContexts
import org.flywaydb.core.internal.logging.slf4j.Slf4jLogCreator
import org.flywaydb.core.api.logging.LogFactory
import repository._

object Database {
  def createTables(tx: doobie.Transactor[IO]): IO[Int] = SqlStatements.ddl.run.transact(tx)

  def transactor[F[_]: Async: ContextShift](config: DatabaseConfig): F[Transactor[F]] = {
    val F = implicitly[Async[F]]
    F.delay {
      Transactor
        .fromDriverManager[F](config.driver, config.url, config.user, config.password)
    }
  }

  /**
    * Provides a transactor using the given config.
    */
  def hikariTransactor[F[_]: Async: ContextShift](config: DatabaseConfig): Resource[F, HikariTransactor[F]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[F](32)
      be <- Blocker[F]
      xa <- HikariTransactor.newHikariTransactor[F](
             driverClassName = config.driver,
             url = config.url,
             user = config.user,
             pass = config.password,
             connectEC = ce,
             blocker = be
           )
    } yield xa

  def migrate[F[_]](config: DatabaseConfig)(implicit F: Sync[F]): F[Unit] = F.delay {

    LogFactory.setLogCreator(new Slf4jLogCreator())
    val cl = Class.forName(config.driver).getClassLoader

    Flyway
      .configure(cl)
      .dataSource(
        config.url,
        config.user,
        config.password
      )
      .load()
      .migrate()
    ()
  }
}
