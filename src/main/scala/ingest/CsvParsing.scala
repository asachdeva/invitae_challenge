package ingest

import java.nio.file.Paths
import java.util.concurrent.Executors
import cats.effect._
import fs2._
import module.CohortModule._
import scala.concurrent._
import scala.util.Try

trait CsvParsing {
  implicit val cs: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.Implicits.global)

  val blockingPool: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())
  val blocker: Blocker = Blocker.liftExecutionContext(blockingPool)
  val blockingExecutionContext: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(2))

  def csvParser[F[_]]: Pipe[F, Byte, List[String]] =
    _.through(text.utf8Decode)
      .through(text.lines)
      .drop(1) // remove headers
      .map(_.split(',').toList) //split on commas

  def orderParser: Stream[IO, Order] =
    io.file
      .readAll[IO](Paths.get(Paths.get("").toAbsolutePath.toString + "/src/main/resources/orders.csv"), blocker, 4096)
      .through(csvParser)
      .map(parseOrder) // parse each line into a valid order
      .unNoneTerminate // terminate when done

  def customerParser: Stream[IO, Customer] =
    io.file
      .readAll[IO](Paths.get(Paths.get("").toAbsolutePath.toString + "/src/main/resources/customers.csv"),
                   blocker,
                   4096)
      .through(csvParser)
      .map(parseCustomer) // parse each line into a valid customer
      .unNoneTerminate // terminate when done

  val parseCustomer: List[String] => Option[Customer] = {
    case id :: created :: Nil =>
      Try(
        Customer(
          id.toInt,
          created
        )
      ).toOption
    case _ => None
  }

  val parseOrder: List[String] => Option[Order] = {
    case id :: order_number :: user_id :: created :: Nil =>
      Try(
        Order(
          id.toInt,
          order_number.toInt,
          user_id.toInt,
          created
        )
      ).toOption
    case _ => None
  }
}
