package ingest

import java.time.LocalDate
import java.util.Calendar

import org.joda.time.{ DateTime, DateTimeZone }

trait CohortDateUtils {

  // create week from year, month, day and timezone info
  def getWeek(year: Int, month: Int, day: Int, timeZone: String) = {
    val zone            = DateTimeZone.forID(timeZone)
    val ISOWeekTimeZone = new DateTime(year, month.toInt, day, 12, 0, 0, 0, zone)
    (ISOWeekTimeZone.getWeekyear, ISOWeekTimeZone.getWeekOfWeekyear)
  }

  def toLocalDate(dateTime: DateTime): LocalDate = {
    val dateTimePST = dateTime.withZone(DateTimeZone.forID("Pacific/US"))
    LocalDate.of(dateTimePST.getYear(), dateTimePST.getMonthOfYear(), dateTimePST.getDayOfMonth())
  }

  def getWeekBuckets(year: Int, week: Int, day: Int = 1) = {
    var listOfBuckets: List[(LocalDate, LocalDate)] = List.empty
    val calendar                                    = Calendar.getInstance()
    for (i <- 0 to 7) {
      calendar.setWeekDate(year, week - i, day)
      val startOfBucket = new DateTime(calendar.getTime)
      val endOfBucket   = startOfBucket.plusDays(7)
      listOfBuckets = listOfBuckets :+ ((toLocalDate(startOfBucket), toLocalDate(endOfBucket)))
    }
    listOfBuckets
  }

}
