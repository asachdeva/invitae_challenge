package module

import doobie.util._

object CohortModule {
  sealed trait CohortModel
  final case class Customer(id: Int, created: String)                               extends CohortModel
  final case class Order(id: Int, order_number: Int, user_id: Int, created: String) extends CohortModel

  implicit val orderRead: Read[Order] =
    Read[(Int, Int, Int, String)].map { case (x, y, z, s) => Order(x, y, z, s) }

  implicit val orderWrite: Write[Order] =
    Write[(Int, Int, Int, String)].contramap(p => (p.id, p.order_number, p.user_id, p.created))
}
