import cats.effect._
import config._
import cats.implicits._
import db._
import service._
import org.http4s.server.blaze._
import org.http4s.implicits._
import org.http4s.server.Router
import repository._
import ingest._

object Boot extends IOApp with CsvParsing with CohortDateUtils {
  override implicit val cs: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.Implicits.global)

  def ingestAnalyze[F[_]: ContextShift: ConcurrentEffect: Timer]: Resource[F, ExitCode] =
    for {
      config                <- Resource.liftF(Config.load[F]())
      tx                    <- Database.hikariTransactor[F](config.database)
      cohortRepository      <- Resource.liftF(CohortRepositoryF.apply[F](tx))
      cohortAnalysisService = new CohortAnalysisService().service(cohortRepository)
      httpApp               = Router("/" -> cohortAnalysisService).orNotFound
      _                     <- Resource.liftF(Database.migrate(config.database))

      // Data Ingest
      customers = customerParser.compile.toList.unsafeRunSync
      _         <- Resource.liftF(cohortRepository.insertManyCustomers(customers))
      orders    = orderParser.compile.toList.unsafeRunSync
      _         <- Resource.liftF(cohortRepository.insertManyOrders(orders))

      // Last Week Range
      latestCustomerDate <- Resource.liftF(cohortRepository.getNewestCreatedCustomer())
      endOfCohortWeekRange = getWeek(latestCustomerDate.get.getYear,
                                     latestCustomerDate.get.getMonth.getValue,
                                     latestCustomerDate.get.getDayOfMonth,
                                     "US/Pacific")

      weekBuckets     = getWeekBuckets(endOfCohortWeekRange._1, endOfCohortWeekRange._2)
      customerBuckets <- Resource.liftF(cohortRepository.getCustomersInDateRange(weekBuckets.head))
      _               = println(customerBuckets)

      exitCode <- Resource.liftF(
                   BlazeServerBuilder[F]
                     .bindHttp(config.server.port, config.server.host)
                     .withHttpApp(httpApp)
                     .serve
                     .compile
                     .drain
                     .as(ExitCode.Success)
                 )
    } yield exitCode

  override def run(args: List[String]): IO[ExitCode] =
    ingestAnalyze.use(IO.pure) // Use IO as the effect type
}
