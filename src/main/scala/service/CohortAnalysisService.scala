package service

import cats.effect._
import io.circe._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.dsl.impl._
import repository._

final class CohortAnalysisService[F[_]] extends Http4sDsl[F] {
  implicit val cs: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.Implicits.global)
  private def generateAnalysisJsonForTimeZone(
      cohortRepository: CohortRepository[F],
      timezone: String
  ): Json = {
    val fieldList =
      List(
        ("cohort", Json.fromString("cohort"))
      )
    Json.fromFields(fieldList)
  }

  def service(cohortRepository: CohortRepository[F])(implicit F: ConcurrentEffect[F]): HttpRoutes[F] =
    HttpRoutes
      .of[F] {
        case GET -> Root / "cohort" / "analysis" :? timezoneParam(tz) =>
          if (!tz.isEmpty) {
            Ok(generateAnalysisJsonForTimeZone(cohortRepository, tz))
          } else BadRequest()
      }
}

object timezoneParam extends QueryParamDecoderMatcher[String]("tz")
