package repository

import java.time.LocalDate

import cats.data.NonEmptyList
import module.CohortModule._

trait CohortRepository[F[_]] {

  def insertManyCustomers(ps: List[Customer]): F[Boolean]
  def insertManyOrders(ps: List[Order]): F[Boolean]

  def getOldestCreatedCustomer(): F[Option[LocalDate]]
  def getNewestCreatedCustomer(): F[Option[LocalDate]]
  def getNewestOrder(): F[Option[LocalDate]]

  def getCustomersInDateRange(dateRange: (LocalDate, LocalDate)): F[Option[List[Customer]]]
  def getOrdersForCustomers(customers: NonEmptyList[Int]): fs2.Stream[F, Order]
}
