package repository

import java.sql.Date
import java.time.LocalDate

import cats.data.NonEmptyList
import cats.effect._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.util.update.Update0
import module.CohortModule._

final class CohortRepositoryF[F[_]: ConcurrentEffect](tx: Transactor[F]) extends CohortRepository[F] {
  override def insertManyCustomers(ps: List[Customer]): F[Boolean] = {
    val sql = "insert into customers (id, created) values (?, ?)"
    Update[Customer](sql).updateMany(ps).transact(tx).map(num => num == 1)
  }

  override def insertManyOrders(ps: List[Order]): F[Boolean] = {
    val sql = "insert into orders (id, order_number, user_id, created) values (?, ?, ?, ?)"
    Update[Order](sql).updateMany(ps).transact(tx).map(num => num == 1)
  }

  override def getOldestCreatedCustomer(): F[Option[LocalDate]] =
    SqlStatements.getOldestCustomer().map(_.toLocalDate).option.transact(tx)

  override def getNewestCreatedCustomer(): F[Option[LocalDate]] =
    SqlStatements.getNewestCustomer().map(_.toLocalDate).option.transact(tx)

  override def getNewestOrder(): F[Option[LocalDate]] =
    SqlStatements.getNewestOrder().map(_.toLocalDate).option.transact(tx)

  override def getOrdersForCustomers(customers: NonEmptyList[Int]): fs2.Stream[F, Order] =
    SqlStatements.getOrdersForCustomers(customers).query[Order].stream.transact(tx)

  override def getCustomersInDateRange(dateRange: (LocalDate, LocalDate)): F[Option[List[Customer]]] =
    SqlStatements.customersIn(dateRange._1, dateRange._2).option.transact(tx)
}

object CohortRepositoryF {
  def apply[F[_]](tx: Transactor[F])(implicit F: ConcurrentEffect[F]): F[CohortRepositoryF[F]] =
    F.pure { new CohortRepositoryF(tx) }
}

object SqlStatements {
  val ddl: Update0 =
    sql"""CREATE TABLE customers (
    id  INT PRIMARY KEY NOT NULL,
    created TIMESTAMP NOT NULL
);
CREATE INDEX idx_customer_created ON customers(created);
CREATE TABLE orders (
	id INT PRIMARY KEY NOT NULL,
	order_number INT NOT NULL,
	user_id INT NOT NULL,
	created TIMESTAMP NOT NULL,
	FOREIGN KEY(user_id) REFERENCES customers(id)
);
CREATE INDEX idx_order_created ON orders(created);""".update

  def addCustomer(id: Int, created: Date): Update0 =
    sql"""INSERT INTO customers (id, created) VALUES ($id, $created)""".update

  def addOrder(id: Int, order_number: Int, user_id: Int, created: Date): Update0 =
    sql"""INSERT INTO orders (id, order_number, user_id, created) VALUES ($id, $order_number, $user_id, $created)""".update

  def getOldestCustomer(): Query0[Date] =
    sql"""SELECT top(1) created from CUSTOMERS ORDER BY created"""
      .query[Date]

  def customersIn(from: LocalDate, to: LocalDate): Query0[List[Customer]] =
    sql"""SELECT * FROM CUSTOMERS WHERE created > $from and created < $to """.query[List[Customer]]

  def getNewestCustomer(): Query0[Date] =
    sql"""SELECT top(1) created from CUSTOMERS ORDER BY created DESC"""
      .query[Date]

  def getNewestOrder(): Query0[Date] =
    sql"""SELECT created from ORDERS ORDER BY created DESC"""
      .query[Date]

  def getOrdersForCustomers(customers: NonEmptyList[Int]) =
    fr"""SELECT FROM ORDERS where""" ++ Fragments.in(fr"user_id", customers) ++ fr"""ORDER BY created"""
}
