import sbt._

object Dependencies {

  object Versions {
    val cats                = "2.0.0-RC1"
    val catsPar             = "1.0.0-RC1"
    val catsEffect          = "2.0.0-RC1"
    val circe               = "0.12.0-RC3"
    val doobie              = "0.8.0-RC1"
    val flyway              = "6.0.0-beta2"
    val fs2                 = "1.1.0-M1"
    val h2                  = "1.4.197"
    val http4s              = "0.21.0-M4"
    val jodaTime            = "2.10.3"
    val log4cats            = "1.0.0-RC1"
    val pureConfig          = "0.11.1"

    val betterMonadicFor    = "0.3.1"

    val logback             = "1.2.3"

    val scalaCheck          = "1.14.0"
    val scalaMock           = "4.4.0"
    val scalaTest           = "3.0.8"
  }

  object Libraries {
    def circe(artifact: String): ModuleID = "io.circe"      %% artifact % Versions.circe
    def doobie(artifact: String): ModuleID = "org.tpolecat" %% artifact % Versions.doobie
    def http4s(artifact: String): ModuleID = "org.http4s"   %% artifact % Versions.http4s

    lazy val cats                = "org.typelevel"         %% "cats-core"                  % Versions.cats
    lazy val catsPar             = "io.chrisdavenport"     %% "cats-par"                   % Versions.catsPar
    lazy val catsEffect          = "org.typelevel"         %% "cats-effect"                % Versions.catsEffect
    
    lazy val circeCore           = circe("circe-core")
    lazy val circeGeneric        = circe("circe-generic")
    lazy val circeGenericExt     = circe("circe-generic-extras")
    lazy val circeLiteral        = circe("circe-literal")
    lazy val circeParser         = circe("circe-parser")
    
    lazy val doobieCore          = doobie("doobie-core")
    lazy val doobieH2            = doobie("doobie-h2")
    lazy val doobieHikari        = doobie("doobie-hikari")
    lazy val doobieTest          = doobie("doobie-scalatest")
    
    lazy val fs2                 = "co.fs2"                %% "fs2-core"                   % Versions.fs2
    lazy val fs2IO               = "co.fs2"                %% "fs2-io"                     % Versions.fs2
    lazy val flyway              = "org.flywaydb"          %  "flyway-core"                % Versions.flyway
    lazy val h2                  = "com.h2database"        %  "h2"                         % Versions.h2
    
    lazy val http4sDsl           = http4s("http4s-dsl")
    lazy val http4sServer        = http4s("http4s-blaze-server")
    lazy val http4sCirce         = http4s("http4s-circe")
    lazy val http4sClient        = http4s("http4s-blaze-client")

    lazy val jodaTime            = "joda-time"             % "joda-time"                   % Versions.jodaTime
    lazy val log4cats            = "io.chrisdavenport"     %% "log4cats-slf4j"             % Versions.log4cats
    
    lazy val pureConfig          = "com.github.pureconfig" %% "pureconfig"                 % Versions.pureConfig

    // Compiler plugins
    lazy val betterMonadicFor    = "com.olegpy"            %% "better-monadic-for"         % Versions.betterMonadicFor

    // Runtime
    lazy val logback             = "ch.qos.logback"        %  "logback-classic"            % Versions.logback
   
    // Test
    lazy val catsEffectLaws      = "org.typelevel"         %% "cats-effect-laws"           % Versions.catsEffect
    lazy val scalaCheck          = "org.scalacheck"        %% "scalacheck"                 % Versions.scalaCheck
    lazy val scalaMock           = "org.scalamock"         %% "scalamock"                  % Versions.scalaMock    
    lazy val scalaTest           = "org.scalatest"         %% "scalatest"                  % Versions.scalaTest
  }
}